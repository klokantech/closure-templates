"""
Closure Templates (Soy)
~~~~~~~~~~~~~~~~~~~~~~~

What are Closure Templates?
---------------------------

Closure Templates are a client- and server-side templating system that helps you
dynamically build reusable HTML and UI elements. They are easy to learn and
customizable to fit your application's needs. Closure Templates support
JavaScript, Java and Python and use a data model and expression syntax that
works for any language. You can also use the built-in message support to
easily localize your applications.

What are the benefits of using Closure Templates?
-------------------------------------------------

 * Convenience. Closure Templates provide an easy way to build pieces of HTML
   for your application's UI and help you separate application logic from display.

 * Language-neutral. Closure Templates work with JavaScript, Java or Python. You can
   write one template and share it between your client- and server-side code.

 * Client-side speed. Closure Templates are compiled to efficient JavaScript
   functions for maximum client-side performance.

 * Easy to read. You can clearly see the structure of the output HTML from the
   structure of the template code. Messages for translation are inline for extra
   readability.

 * Designed for programmers. Templates are simply functions that can call each other.
   The syntax includes constructs familiar to programmers. You can put multiple
   templates in one source file.

 * A tool, not a framework. Works well in any web application environment in
   conjunction with any libraries, frameworks, or other tools.

 * Battle-tested. Closure Templates are used extensively in some of the largest
   web applications in the world, including Gmail and Google Docs.

How do I start?
---------------

 * Work through `Hello World Using Python`_.
 * Work through `Hello World Using JavaScript`_.
 * Work through `Hello World Using Java`_.
 * Read the `Documentation`_.

:: _Hello World Using Python: http://code.google.com/closure/templates/docs/helloworld_python.html
:: _Hello World Using JavaScript: http://code.google.com/closure/templates/docs/helloworld_js.html
:: _Hello World Using Java: http://code.google.com/closure/templates/docs/helloworld_java.html
:: _Documentation: http://code.google.com/closure/templates/docs/overview.html

"""

from setuptools import setup, find_packages

setup(
    name='Soy',
    version='2012-12-21',
    url='https://code.google.com/p/closure-templates/',
    license='Apache',
    author='Lukas Lalinsky',
    author_email='lukas@oxygene.sk',
    description='Client- and server-side templating system for JavaScript, Java and Python.',
    long_description=__doc__,
    packages=find_packages('.'),
    zip_safe=False,
    platforms='any',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Operating System :: OS Independent',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Text Processing :: Markup :: HTML',
        'License :: OSI Approved :: Apache Software License',
    ],
)

