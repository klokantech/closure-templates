/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.sharedpasses;

import com.google.template.soy.soytree.AbstractSoyNodeVisitor;
import com.google.template.soy.soytree.SoyFileSetNode;
import com.google.template.soy.soytree.SoyNode;
import com.google.template.soy.soytree.SoyNode.ParentSoyNode;
import com.google.template.soy.soytree.TemplateDelegateNode;


/**
 * Visitor for determining whether any code in a Soy tree uses delegate packages.
 *
 * @author Lukas Lalinsky
 */
public class IsUsingDelPackagesVisitor extends AbstractSoyNodeVisitor<Boolean> {


  /** Whether any delegate packages have been found so far. */
  private boolean isUsingDelPackages;


  public IsUsingDelPackagesVisitor() {
    isUsingDelPackages = false;
  }


  /**
   * Runs this pass on the given Soy tree.
   */
  public boolean exec(SoyFileSetNode soyTree) {
    visit(soyTree);
    return isUsingDelPackages;
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for specific nodes.


  @Override protected void visitTemplateDelegateNode(TemplateDelegateNode node) {
    if (node.getDelPackageName() != null) {
      isUsingDelPackages = true;
    }
  }


  // -----------------------------------------------------------------------------------------------
  // Fallback implementation.


  @Override protected void visitSoyNode(SoyNode node) {
    if (node instanceof ParentSoyNode<?>) {
      visitChildren((ParentSoyNode<?>) node);
    }
  }

}
