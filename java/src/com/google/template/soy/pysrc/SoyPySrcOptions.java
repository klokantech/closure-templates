/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc;

import com.google.common.base.Preconditions;


/**
 * Compilation options for the Python Src output target (backend).
 *
 * @author Kai Huang
 */
public class SoyPySrcOptions implements Cloneable {


  /**
   * The two supported code styles.
   */
  public static enum CodeStyle {
    STRINGBUILDER, CONCAT;
  }

  /** Whether to enable use of injected data. */
  private boolean isUsingIjData;

  /** Whether to enable use of delegate packages. */
  private boolean isUsingDelPackages;

  /** The output variable code style to use. */
  private CodeStyle codeStyle;

  /**
   * The bidi global directionality as a static value, 1: ltr, -1: rtl, 0: unspecified. If 0, the
   * bidi global directionality will be inferred from the message bundle locale. This is the
   * recommended mode of operation.
   */
  private int bidiGlobalDir;


  public SoyPySrcOptions() {
    isUsingIjData = false;
    codeStyle = CodeStyle.CONCAT;
    bidiGlobalDir = 0;
  }


  /**
   * Sets whether to enable use of injected data (syntax is '$ij.*').
   * @param isUsingIjData The code style to set.
   */
  public void setIsUsingIjData(boolean isUsingIjData) {
    this.isUsingIjData = isUsingIjData;
  }


  /** Returns whether use of injected data is currently enabled. */
  public boolean isUsingIjData() {
    return isUsingIjData;
  }


  /**
   * Sets whether to enable use of delegate packages.
   * @param isUsingDelPackages The code style to set.
   */
  public void setIsUsingDelPackages(boolean isUsingDelPackages) {
    this.isUsingDelPackages = isUsingDelPackages;
  }


  /** Returns whether use of delegate packages is currently enabled. */
  public boolean isUsingDelPackages() {
    return isUsingDelPackages;
  }


  /**
   * Sets the output variable code style to use.
   * @param codeStyle The code style to set.
   */
  public void setCodeStyle(CodeStyle codeStyle) {
    this.codeStyle = codeStyle;
  }


  /** Returns the currently set code style. */
  public CodeStyle getCodeStyle() {
    return codeStyle;
  }


  /**
   * Sets the bidi global directionality to a static value, 1: ltr, -1: rtl, 0: unspecified. If 0,
   * the bidi global directionality will be inferred from the message bundle locale. This is the
   * recommended mode of operation. Thus, THERE IS USUALLY NO NEED TO USE THIS METHOD!
   *
   * @param bidiGlobalDir 1: ltr, -1: rtl, 0: unspecified. Checks that no other value is used.
   */
  public void setBidiGlobalDir(int bidiGlobalDir) {
    Preconditions.checkArgument(
        bidiGlobalDir >= -1 && bidiGlobalDir <= 1,
        "bidiGlobalDir must be 1 for LTR, or -1 for RTL (or 0 to leave unspecified).");
    this.bidiGlobalDir = bidiGlobalDir;
  }


  /**
   * Returns the static bidi global directionality, 1: ltr, -1: rtl, 0: unspecified.
   */
  public int getBidiGlobalDir() {
    return bidiGlobalDir;
  }


  @Override public SoyPySrcOptions clone() {
    try {
      return (SoyPySrcOptions) super.clone();
    } catch (CloneNotSupportedException cnse) {
      throw new RuntimeException("Cloneable interface removed from SoyPySrcOptions.");
    }
  }

}
