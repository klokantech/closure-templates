/*
 * Copyright 2009 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc.restricted;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.template.soy.internal.base.CharEscapers;
import com.google.template.soy.shared.restricted.EscapingConventions;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Utilities for building code for the Python Source backend.
 *
 * <p> Important: This class may only be used in implementing plugins (e.g. functions, directives).
 *
 * @author Lukas Lalinsky
 */
public class PyCodeUtils {

  private PyCodeUtils() {}


  public static final String UTILS_LIB = "soy.utils";

  public static final String DATA_LIB = "soy.data";

  private static final Pattern INT_PATTERN = Pattern.compile("[0-9]+");


  /**
   * Builds a string with Python unicode string literal for this string value (including the surrounding single quotes).
   *
   * @param value The string value to convert.
   * @return A Python string literal.
   */
  public static String genStringLiteral(String value) {
    if (value == null) {
      return "None";
    }
    return "u'" + CharEscapers.pythonStringEscaper().escape(value) + "'";
  }


  /**
   * Builds a string with Python str string literal for this string value (including the surrounding single quotes).
   *
   * @param value The string value to convert.
   * @return A Python string literal.
   */
  public static String genAsciiStringLiteral(String value) {
    if (value == null) {
      return "None";
    }
    // TODO validate that the string is ASCII-only
    return "'" + CharEscapers.pythonStringEscaper().escape(value) + "'";
  }


  /**
   * Builds a string with Python bool literal for this boolean value.
   *
   * @param value The bool value to convert.
   * @return A Python bool literal.
   */
  public static String genBoolLiteral(boolean value) {
    return value ? "True" : "False";
  }


  /**
   * Builds a string with Python int literal for this int value.
   *
   * @param value The int value to convert.
   * @return A Python int literal.
   */
  public static String genIntLiteral(int value) {
    return Integer.toString(value);
  }


  /**
   * Builds a string with Python function call.
   *
   * @param name String with the function name.
   * @param args Strings with the function arguments.
   * @return A string with the whole function call.
   */
  public static String genFunctionCall(String name, String... args) {
    return name + "(" + Joiner.on(", ").join(args) + ")";
  }


  /**
   * Builds a string with Python code that initializes a new string builder.
   * @param initial A string with the Python code for initialization
   * @return A string with the Python code
   */
  public static String genStringBuilder(String initial) {
    return "[" + initial + "]";
  }


  /**
   * Builds a string with Python code that converts a string builder to string.
   * @param sb A string with the string builder variable name
   * @return A string with the Python code
   */
  public static String genStringBuilderToString(String sb) {
    return genFunctionCall(genStringLiteral("") + ".join", sb);
  }


  public static String genIntValue(PyExpr expr) {
    String exprText = expr.getText();
    if (INT_PATTERN.matcher(exprText).matches()) {
      return exprText;
    }
    return genFunctionCall("int", exprText);
  }


  public static String genBoolValue(PyExpr expr) {
    String exprText = expr.getText();
    if (exprText.equals("true")) {
      return "True";
    }
    if (exprText.equals("false")) {
      return "False";
    }
    return genFunctionCall("bool", exprText);
  }


  /**
   * Escapes characters that have special meaning in Python regexes.
   *
   * @param value The string value to escape.
   * @return Escaped string.
   */
  public static String escapePyRegex(String value) {
    return CharEscapers.pythonRegexEscaper().escape(value);
  }

}
