/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc.internal;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.google.template.soy.base.BaseUtils;
import com.google.template.soy.exprtree.ExprRootNode;
import com.google.template.soy.exprtree.Operator;
import com.google.template.soy.pysrc.internal.TranslateToPyExprVisitor.TranslateToPyExprVisitorFactory;
import com.google.template.soy.pysrc.restricted.PyCodeUtils;
import com.google.template.soy.pysrc.restricted.PyExpr;
import com.google.template.soy.pysrc.restricted.PyExprUtils;
import com.google.template.soy.pysrc.restricted.SoyPySrcPrintDirective;
import com.google.template.soy.soytree.AbstractSoyNodeVisitor;
import com.google.template.soy.soytree.CallNode;
import com.google.template.soy.soytree.CallParamContentNode;
import com.google.template.soy.soytree.CssNode;
import com.google.template.soy.soytree.IfCondNode;
import com.google.template.soy.soytree.IfElseNode;
import com.google.template.soy.soytree.IfNode;
import com.google.template.soy.soytree.MsgHtmlTagNode;
import com.google.template.soy.soytree.MsgNode;
import com.google.template.soy.soytree.MsgPlaceholderNode;
import com.google.template.soy.soytree.PrintDirectiveNode;
import com.google.template.soy.soytree.PrintNode;
import com.google.template.soy.soytree.RawTextNode;
import com.google.template.soy.soytree.SoyNode;
import com.google.template.soy.soytree.SoyNode.ParentSoyNode;
import com.google.template.soy.soytree.SoySyntaxExceptionUtils;
import com.google.template.soy.soytree.TemplateNode;

import java.util.Deque;
import java.util.List;
import java.util.Map;


/**
 * Visitor for generating Python expressions for parse tree nodes.
 *
 * <p> Important: Do not use outside of Soy code (treat as superpackage-private).
 *
 * <p> Precondition: MsgNode should not exist in the tree.
 *
 * @author Kai Huang
 */
public class GenPyExprsVisitor extends AbstractSoyNodeVisitor<List<PyExpr>> {


  /**
   * Injectable factory for creating an instance of this class.
   */
  public static interface GenPyExprsVisitorFactory {

    /**
     * @param localVarTranslations The current stack of replacement Python expressions for the local
     *     variables (and foreach-loop special functions) current in scope.
     */
    public GenPyExprsVisitor create(Deque<Map<String, PyExpr>> localVarTranslations);
  }


  /** Map of all SoyPySrcPrintDirectives (name to directive). */
  Map<String, SoyPySrcPrintDirective> soyPySrcDirectivesMap;

  /** Factory for creating an instance of TranslateToPyExprVisitor. */
  private final TranslateToPyExprVisitorFactory translateToPyExprVisitorFactory;

  /** Instance of GenCallCodeUtils to use. */
  private final GenCallCodeUtils genCallCodeUtils;

  /** The IsComputableAsPyExprsVisitor used by this instance (when needed). */
  private final IsComputableAsPyExprsVisitor isComputableAsPyExprsVisitor;

  /** Factory for creating an instance of GenPyExprsVisitor. */
  private final GenPyExprsVisitorFactory genPyExprsVisitorFactory;

  /** The current stack of replacement Python expressions for the local variables (and foreach-loop
   *  special functions) current in scope. */
  private final Deque<Map<String, PyExpr>> localVarTranslations;

  /** List to collect the results. */
  private List<PyExpr> pyExprs;


  /**
   * @param soyPySrcDirectivesMap Map of all SoyPySrcPrintDirectives (name to directive).
   * @param translateToPyExprVisitorFactory Instance of TranslateToPyExprVisitorFactory to use.
   * @param genCallCodeUtils Instance of GenCallCodeUtils to use.
   * @param isComputableAsPyExprsVisitor The IsComputableAsPyExprsVisitor used by this instance
   *     (when needed).
   * @param genPyExprsVisitorFactory Factory for creating an instance of GenPyExprsVisitor.
   * @param localVarTranslations The current stack of replacement Python expressions for the local
   *     variables (and foreach-loop special functions) current in scope.
   */
  @AssistedInject
  GenPyExprsVisitor(
      Map<String, SoyPySrcPrintDirective> soyPySrcDirectivesMap,
      TranslateToPyExprVisitorFactory translateToPyExprVisitorFactory,
      GenCallCodeUtils genCallCodeUtils, IsComputableAsPyExprsVisitor isComputableAsPyExprsVisitor,
      GenPyExprsVisitorFactory genPyExprsVisitorFactory,
      @Assisted Deque<Map<String, PyExpr>> localVarTranslations) {
    this.soyPySrcDirectivesMap = soyPySrcDirectivesMap;
    this.translateToPyExprVisitorFactory = translateToPyExprVisitorFactory;
    this.genCallCodeUtils = genCallCodeUtils;
    this.isComputableAsPyExprsVisitor = isComputableAsPyExprsVisitor;
    this.genPyExprsVisitorFactory = genPyExprsVisitorFactory;
    this.localVarTranslations = localVarTranslations;
  }


  @Override public List<PyExpr> exec(SoyNode node) {
    Preconditions.checkArgument(isComputableAsPyExprsVisitor.exec(node));
    pyExprs = Lists.newArrayList();
    visit(node);
    return pyExprs;
  }


  /**
   * Executes this visitor on the children of the given node, without visiting the given node
   * itself.
   */
  public List<PyExpr> execOnChildren(ParentSoyNode<?> node) {
    Preconditions.checkArgument(isComputableAsPyExprsVisitor.execOnChildren(node));
    pyExprs = Lists.newArrayList();
    visitChildren(node);
    return pyExprs;
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for specific nodes.


  @Override protected void visitTemplateNode(TemplateNode node) {
    visitChildren(node);
  }


  /**
   * Example:
   * <pre>
   *   I'm feeling lucky!
   * </pre>
   * generates
   * <pre>
   *   u'I\'m feeling lucky!'
   * </pre>
   */
  @Override protected void visitRawTextNode(RawTextNode node) {
    pyExprs.add(new PyExpr(
        PyCodeUtils.genStringLiteral(node.getRawText()),
        Integer.MAX_VALUE));
  }


  @Override protected void visitMsgPlaceholderNode(MsgPlaceholderNode node) {
    visitChildren(node);
  }


  /**
   * Example:
   * <xmp>
   *   <a href="{$url}">
   * </xmp>
   * might generate
   * <xmp>
   *   u'<a href="' + data['url'] + u'">'
   * </xmp>
   */
  @Override protected void visitMsgHtmlTagNode(MsgHtmlTagNode node) {
    visitChildren(node);
  }


  /**
   * Example:
   * <pre>
   *   {$boo.foo}
   *   {$goo.moo + 5}
   * </pre>
   * might generate
   * <pre>
   *   data['boo']['foo']
   *   data['goo']['moo'] + 5
   * </pre>
   */
  @Override protected void visitPrintNode(PrintNode node) {

    PyExpr pyExpr = translateToPyExprVisitorFactory.create(localVarTranslations).exec(node.getExprUnion().getExpr());

    // Process directives.
    for (PrintDirectiveNode directiveNode : node.getChildren()) {

      // Get directive.
      SoyPySrcPrintDirective directive = soyPySrcDirectivesMap.get(directiveNode.getName());
      if (directive == null) {
        throw SoySyntaxExceptionUtils.createWithNode(
            "Failed to find SoyPySrcPrintDirective with name '" + directiveNode.getName() + "'" +
                " (tag " + node.toSourceString() + ")",
            directiveNode);
      }

      // Get directive args.
      List<ExprRootNode<?>> args = directiveNode.getArgs();
      if (! directive.getValidArgsSizes().contains(args.size())) {
        throw SoySyntaxExceptionUtils.createWithNode(
            "Print directive '" + directiveNode.getName() + "' used with the wrong number of" +
                " arguments (tag " + node.toSourceString() + ").",
            directiveNode);
      }

      // Translate directive args.
      List<PyExpr> argsPyExprs = Lists.newArrayListWithCapacity(args.size());
      for (ExprRootNode<?> arg : args) {
        argsPyExprs.add(translateToPyExprVisitorFactory.create(localVarTranslations).exec(arg));
      }

      // Apply directive.
      pyExpr = directive.applyForPySrc(pyExpr, argsPyExprs);
    }

    pyExprs.add(pyExpr);
  }


  /**
   * Note: We would only see a CssNode if the css-handling scheme is BACKEND_SPECIFIC.
   *
   * Example:
   * <pre>
   *   {css selected-option}
   *   {css $foo, bar}
   * </pre>
   * might generate
   * <pre>
   *   goog.getCssName('selected-option')
   *   goog.getCssName(opt_data.foo, 'bar')
   * </pre>
   */
  @Override protected void visitCssNode(CssNode node) {

    StringBuilder sb = new StringBuilder();
    sb.append("goog.getCssName(");

    ExprRootNode<?> componentNameExpr = node.getComponentNameExpr();
    if (componentNameExpr != null) {
      PyExpr basePyExpr =
          translateToPyExprVisitorFactory.create(localVarTranslations).exec(componentNameExpr);
      sb.append(basePyExpr.getText()).append(", ");
    }

    sb.append('\'').append(node.getSelectorText()).append("')");

    pyExprs.add(new PyExpr(sb.toString(), Integer.MAX_VALUE));
  }


  /**
   * Example:
   * <pre>
   *   {if $boo}
   *     AAA
   *   {elseif $foo}
   *     BBB
   *   {else}
   *     CCC
   *   {/if}
   * </pre>
   * might generate
   * <pre>
   *   AAA if data['boo'] else BBB if data['foo'] else CCC
   * </pre>
   */
  @Override protected void visitIfNode(IfNode node) {

    // Create another instance of this visitor class for generating Python expressions from children.
    GenPyExprsVisitor genPyExprsVisitor = genPyExprsVisitorFactory.create(localVarTranslations);

    StringBuilder pyExprTextSb = new StringBuilder();

    boolean hasElse = false;
    for (SoyNode child : node.getChildren()) {

      if (child instanceof IfCondNode) {
        IfCondNode icn = (IfCondNode) child;

        List<PyExpr> condBlockPyExprs = genPyExprsVisitor.exec(icn);
        pyExprTextSb.append(PyExprUtils.concatPyExprs(condBlockPyExprs).getText());

        PyExpr condPyExpr = translateToPyExprVisitorFactory.create(localVarTranslations).exec(icn.getExprUnion().getExpr());
        pyExprTextSb.append(" if ").append(condPyExpr.getText()).append(" else ");

      } else if (child instanceof IfElseNode) {
        hasElse = true;
        IfElseNode ien = (IfElseNode) child;

        List<PyExpr> elseBlockPyExprs = genPyExprsVisitor.exec(ien);
        pyExprTextSb.append(PyExprUtils.concatPyExprs(elseBlockPyExprs).getText());

      } else {
        throw new AssertionError();
      }
    }

    if (!hasElse) {
      pyExprTextSb.append("u''");
    }

    pyExprs.add(new PyExpr(pyExprTextSb.toString(), Operator.CONDITIONAL.getPrecedence()));
  }


  @Override protected void visitIfCondNode(IfCondNode node) {
    visitChildren(node);
  }


  @Override protected void visitIfElseNode(IfElseNode node) {
    visitChildren(node);
  }


  /**
   * Example:
   * <pre>
   *   {call name="some.func" data="all" /}
   *   {call name="some.func" data="$boo.foo" /}
   *   {call name="some.func"}
   *     {param key="goo" value="$moo" /}
   *   {/call}
   *   {call name="some.func" data="$boo"}
   *     {param key="goo"}Blah{/param}
   *   {/call}
   * </pre>
   * might generate
   * <pre>
   *   soy.some.func(data)
   *   soy.some.func(data['boo']['foo'])
   *   soy.some.func({'goo': data['moo']})
   *   soy.some.func(soy._augmentMap(data['boo'], {'goo': u'Blah'}))
   * </pre>
   */
  @Override protected void visitCallNode(CallNode node) {
    pyExprs.add(genCallCodeUtils.genCallExpr(node, localVarTranslations));
  }


  @Override protected void visitCallParamContentNode(CallParamContentNode node) {
    visitChildren(node);
  }

}
