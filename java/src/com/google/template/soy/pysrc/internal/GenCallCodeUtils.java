/*
 * Copyright 2009 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc.internal;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.template.soy.base.BaseUtils;
import com.google.template.soy.exprtree.ExprRootNode;
import com.google.template.soy.pysrc.SoyPySrcOptions;
import com.google.template.soy.pysrc.internal.TranslateToPyExprVisitor.TranslateToPyExprVisitorFactory;
import com.google.template.soy.pysrc.internal.GenPyExprsVisitor.GenPyExprsVisitorFactory;
import com.google.template.soy.pysrc.restricted.PyCodeUtils;
import com.google.template.soy.pysrc.restricted.PyExpr;
import com.google.template.soy.pysrc.restricted.PyExprUtils;
import com.google.template.soy.pysrc.restricted.SoyPySrcPrintDirective;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.IsUsingDelPackages;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.IsUsingIjData;
import com.google.template.soy.soytree.CallBasicNode;
import com.google.template.soy.soytree.CallDelegateNode;
import com.google.template.soy.soytree.CallNode;
import com.google.template.soy.soytree.CallParamContentNode;
import com.google.template.soy.soytree.CallParamNode;
import com.google.template.soy.soytree.CallParamValueNode;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;


/**
 * Utilities for generating Python code for calls.
 *
 * @author Kai Huang
 */
class GenCallCodeUtils {


  /** All registered Python print directives. */
  private final Map<String, SoyPySrcPrintDirective> soyPySrcDirectivesMap;

  /** The options for generating Python source code. */
  private final SoyPySrcOptions pySrcOptions;

  /** Whether any of the Soy code uses injected data. */
  private final boolean isUsingIjData;

  /** Whether any of the Soy code uses delegate packages. */
  private final boolean isUsingDelPackages;

  /** Factory for creating an instance of TranslateToPyExprVisitor. */
  private final TranslateToPyExprVisitorFactory translateToPyExprVisitorFactory;

  /** The IsComputableAsPyExprsVisitor used by this instance. */
  private final IsComputableAsPyExprsVisitor isComputableAsPyExprsVisitor;

  /** Factory for creating an instance of GenPyExprsVisitor. */
  private final GenPyExprsVisitorFactory genPyExprsVisitorFactory;


  /**
   * @param pySrcOptions The options for generating Python source code.
   * @param isUsingIjData Whether any of the Soy code uses injected data.
   * @param isUsingDelPackages Whether any of the Soy code uses delegate packages.
   * @param translateToPyExprVisitorFactory Instance of TranslateToPyExprVisitorFactory to use.
   * @param isComputableAsPyExprsVisitor The IsComputableAsPyExprsVisitor to be used.
   * @param genPyExprsVisitorFactory Factory for creating an instance of GenPyExprsVisitor.
   */
  @Inject
  GenCallCodeUtils(
      Map<String, SoyPySrcPrintDirective> soyPySrcDirectivesMap, SoyPySrcOptions pySrcOptions,
      @IsUsingIjData boolean isUsingIjData,
      @IsUsingDelPackages boolean isUsingDelPackages,
      TranslateToPyExprVisitorFactory translateToPyExprVisitorFactory,
      IsComputableAsPyExprsVisitor isComputableAsPyExprsVisitor,
      GenPyExprsVisitorFactory genPyExprsVisitorFactory) {
    this.pySrcOptions = pySrcOptions;
    this.isUsingIjData = isUsingIjData;
    this.isUsingDelPackages = isUsingDelPackages;
    this.translateToPyExprVisitorFactory = translateToPyExprVisitorFactory;
    this.isComputableAsPyExprsVisitor = isComputableAsPyExprsVisitor;
    this.genPyExprsVisitorFactory = genPyExprsVisitorFactory;
    this.soyPySrcDirectivesMap = soyPySrcDirectivesMap;
  }


  /**
   * Generates the Python expression for a given call (the version that doesn't pass a StringBuilder).
   *
   * <p> Important: If there are CallParamContentNode children whose contents are not computable as
   * Python expressions, then this function assumes that, elsewhere, code has been generated to define
   * their respective 'param<n>' temporary variables.
   *
   * @see #genCallExprHelper for code gen examples.
   *
   * @param callNode The call to generate code for.
   * @param localVarTranslations The current stack of replacement Python expressions for the local
   *     variables (and foreach-loop special functions) current in scope.
   * @return The Python expression for the call (the version that doesn't pass a StringBuilder).
   */
  public PyExpr genCallExpr(
      CallNode callNode, Deque<Map<String, PyExpr>> localVarTranslations) {
    return genCallExprHelper(callNode, localVarTranslations, null);
  }


  /**
   * Generates the Python statement for a given call (the version that passes a StringBuilder) and
   * appends it to the given pyCodeBuilder. This method is only applicable for code style
   * 'stringbuilder'.
   *
   * <p> Important: If there are CallParamContentNode children whose contents are not computable as
   * Python expressions, then this function assumes that, elsewhere, code has been generated to define
   * their respective 'param<n>' temporary variables.
   *
   * @see #genCallExprHelper for code gen examples.
   *
   * @param pyCodeBuilder The code builder to append the call to.
   * @param callNode The call to generate code for.
   * @param localVarTranslations The current stack of replacement Python expressions for the local
   *     variables (and foreach-loop special functions) current in scope.
   */
  public void genAndAppendCallStmt(
      PyCodeBuilder pyCodeBuilder, CallNode callNode,
      Deque<Map<String, PyExpr>> localVarTranslations) {

    if (pySrcOptions.getCodeStyle() != SoyPySrcOptions.CodeStyle.STRINGBUILDER) {
      throw new AssertionError();
    }

    PyExpr callExpr =
        genCallExprHelper(callNode, localVarTranslations, pyCodeBuilder.getOutputVarName());
    pyCodeBuilder.indent().append(callExpr.getText(), "\n");
  }


  /**
   * Private helper for {@code genCallExpr()} and {@code genAndAppendCallStmt()}.
   *
   * <p> Important: If there are CallParamContentNode children whose contents are not computable as
   * Python expressions, then this function assumes that, elsewhere, code has been generated to define
   * their respective 'param<n>' temporary variables.
   *
   * <p> Here are five example calls:
   * <pre>
   *   {call some.func data="all" /}
   *   {call some.func data="$boo.foo" /}
   *   {call some.func}
   *     {param goo = $moo /}
   *   {/call}
   *   {call some.func data="$boo"}
   *     {param goo}Blah{/param}
   *   {/call}
   *   {call some.func}
   *     {param goo}
   *       {for $i in range(3)}{$i}{/for}
   *     {/param}
   *   {/call}
   * </pre>
   * Their respective generated calls might be the following:
   * <pre>
   *   some.func(opt_data)
   *   some.func(opt_data.boo.foo)
   *   some.func({goo: opt_data.moo})
   *   some.func(soy.$$augmentMap(opt_data.boo, {goo: 'Blah'}))
   *   some.func({goo: param65})
   * </pre>
   * Note that in the last case, the param content is not computable as Python expressions, so we assume
   * that code has been generated to define the temporary variable 'param<n>'.
   *
   * @param callNode The call to generate code for.
   * @param localVarTranslations The current stack of replacement Python expressions for the local
   *     variables (and foreach-loop special functions) current in scope.
   * @param outputVarNameForStringbuilder If set to null, then this method generates a call
   *     expression that returns the output. If nonnull, then this method generates a a call
   *     expression that passes this given stringbuilder object to receive the output. (Note that if
   *     this param is nonnull, then this method will assume the code style is stringbuilder without
   *     checking it.)
   * @return The Python expression for the call (the version that doesn't pass a StringBuilder).
   */
  private PyExpr genCallExprHelper(
      CallNode callNode, Deque<Map<String, PyExpr>> localVarTranslations,
      @Nullable String outputVarNameForStringbuilder) {

    PyExpr objToPass = genObjToPass(callNode, localVarTranslations);

    // Build the Python expr text for the callee.
    String calleeExprText;
    if (callNode instanceof CallBasicNode) {
      // Case 1: Basic call.
      CallBasicNode callBasicNode = (CallBasicNode) callNode;
      calleeExprText = PyCodeUtils.genFunctionCall(
          "registry.lookup",
          PyCodeUtils.genAsciiStringLiteral(callBasicNode.getCalleeName()));
    } else {
      // Case 2: Delegate call.
      CallDelegateNode callDelegateNode = (CallDelegateNode) callNode;
      ExprRootNode<?> variantSoyExpr = callDelegateNode.getDelCalleeVariantExpr();
      String variantPyExprText;
      if (variantSoyExpr == null) {
        // Case 2a: Delegate call with empty variant.
        variantPyExprText = PyCodeUtils.genStringLiteral("");
      } else {
        // Case 2b: Delegate call with variant expression.
        PyExpr variantPyExpr =
            translateToPyExprVisitorFactory.create(localVarTranslations).exec(variantSoyExpr);
        variantPyExprText = variantPyExpr.getText();
      }
      List<String> args = new ArrayList<String>();
      args.add(PyCodeUtils.genAsciiStringLiteral(callDelegateNode.getDelCalleeName()));
      args.add(variantPyExprText);
      args.add(PyCodeUtils.genBoolLiteral(callDelegateNode.allowsEmptyDefault()));
      if (isUsingDelPackages) {
        args.add("packages=delPackages");
      }
      calleeExprText = PyCodeUtils.genFunctionCall(
          "registry.lookupDelegate", args.toArray(new String[args.size()]));
    }

    // Generate the main call expression.
    String callExprText;
    List<String> args = new ArrayList<String>();
    args.add(objToPass.getText());
    if (outputVarNameForStringbuilder != null) {
      args.add(outputVarNameForStringbuilder);
    }
    if (isUsingIjData) {
      args.add("ijData=ijData");
    }
    if (isUsingDelPackages) {
      args.add("delPackages=delPackages");
    }
    callExprText = PyCodeUtils.genFunctionCall(calleeExprText, args.toArray(new String[args.size()]));
    PyExpr result = new PyExpr(callExprText, Integer.MAX_VALUE);

    // In strict mode, escaping directives may apply to the call site.
    for (String directiveName : callNode.getEscapingDirectiveNames()) {
      SoyPySrcPrintDirective directive = soyPySrcDirectivesMap.get(directiveName);
      Preconditions.checkNotNull(directive,
          "Contextual autoescaping produced a bogus directive: " + directiveName);
      result = directive.applyForPySrc(result, ImmutableList.<PyExpr>of());
    }

    return result;
  }


  /**
   * Generates the Python expression for the object to pass in a given call.
   *
   * <p> Important: If there are CallParamContentNode children whose contents are not computable as
   * Python expressions, then this function assumes that, elsewhere, code has been generated to define
   * their respective 'param<n>' temporary variables.
   *
   * <p> Here are five example calls:
   * <pre>
   *   {call some.func data="all" /}
   *   {call some.func data="$boo.foo" /}
   *   {call some.func}
   *     {param goo = $moo /}
   *   {/call}
   *   {call some.func data="$boo"}
   *     {param goo}Blah{/param}
   *   {/call}
   *   {call some.func}
   *     {param goo}
   *       {for $i in range(3)}{$i}{/for}
   *     {/param}
   *   {/call}
   * </pre>
   * Their respective objects to pass might be the following:
   * <pre>
   *   opt_data
   *   opt_data.boo.foo
   *   {goo: opt_data.moo}
   *   soy.$$augmentMap(opt_data.boo, {goo: 'Blah'})
   *   {goo: param65}
   * </pre>
   * Note that in the last case, the param content is not computable as Python expressions, so we assume
   * that code has been generated to define the temporary variable 'param<n>'.
   *
   * @param callNode The call to generate code for.
   * @param localVarTranslations The current stack of replacement Python expressions for the local
   *     variables (and foreach-loop special functions) current in scope.
   * @return The Python expression for the object to pass in the call.
   */
  public PyExpr genObjToPass(CallNode callNode, Deque<Map<String, PyExpr>> localVarTranslations) {

    // ------ Generate the expression for the original data to pass ------
    PyExpr dataToPass;
    if (callNode.isPassingAllData()) {
      dataToPass = new PyExpr("data", Integer.MAX_VALUE);
    } else if (callNode.isPassingData()) {
      dataToPass = translateToPyExprVisitorFactory.create(localVarTranslations).exec(callNode.getDataExpr());
    } else {
      dataToPass = new PyExpr("None", Integer.MAX_VALUE);
    }

    // ------ Case 1: No additional params ------
    if (callNode.numChildren() == 0) {
      return dataToPass;
    }

    // ------ Build an object literal containing the additional params ------
    StringBuilder paramsObjSb = new StringBuilder();
    paramsObjSb.append('{');

    boolean isFirst = true;
    for (CallParamNode child : callNode.getChildren()) {

      if (isFirst) {
        isFirst = false;
      } else {
        paramsObjSb.append(", ");
      }

      String key = child.getKey();
      paramsObjSb.append(PyCodeUtils.genAsciiStringLiteral(key)).append(": ");

      if (child instanceof CallParamValueNode) {
        CallParamValueNode cpvn = (CallParamValueNode) child;
        PyExpr valuePyExpr = translateToPyExprVisitorFactory.create(localVarTranslations).exec(cpvn.getValueExprUnion().getExpr());
        paramsObjSb.append(valuePyExpr.getText());

      } else {
        CallParamContentNode cpcn = (CallParamContentNode) child;
        PyExpr valuePyExpr;
        if (isComputableAsPyExprsVisitor.exec(cpcn)) {
          valuePyExpr = PyExprUtils.concatPyExprs(
              genPyExprsVisitorFactory.create(localVarTranslations).exec(cpcn));
        } else {
          // This is a param with content that cannot be represented as Python expressions, so we assume
          // that code has been generated to define the temporary variable 'param<n>'.
          String paramExpr = "param" + cpcn.getId();
          if (pySrcOptions.getCodeStyle() == SoyPySrcOptions.CodeStyle.STRINGBUILDER) {
            paramExpr = PyCodeUtils.genStringBuilderToString(paramExpr);
          }
          valuePyExpr = new PyExpr(paramExpr, Integer.MAX_VALUE);
        }

        // If the param node had a content kind specified, it was autoescaped in the
        // corresponding context. Hence the result of evaluating the param block is wrapped
        // in a SanitizedContent instance of the appropriate kind.

        // The expression for the constructor of SanitizedContent of the appropriate kind (e.g.,
        // "SanitizedHtml"), or null if the node has no 'kind' attribute.
        valuePyExpr = PyExprUtils.maybeWrapAsSanitizedContent(cpcn.getContentKind(), valuePyExpr);

        paramsObjSb.append(valuePyExpr.getText());
      }
    }

    paramsObjSb.append('}');

    // ------ Cases 2 and 3: Additional params with and without original data to pass ------
    if (callNode.isPassingData()) {
      return new PyExpr(
          PyCodeUtils.genFunctionCall(
              PyCodeUtils.UTILS_LIB + ".augmentMap",
              dataToPass.getText(), paramsObjSb.toString()),
          Integer.MAX_VALUE);
    } else {
      return new PyExpr(paramsObjSb.toString(), Integer.MAX_VALUE);
    }
  }

}
