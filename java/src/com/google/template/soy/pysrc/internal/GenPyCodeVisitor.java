/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc.internal;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.template.soy.base.SoyFileKind;
import com.google.template.soy.base.SoySyntaxException;
import com.google.template.soy.data.internalutils.NodeContentKinds;
import com.google.template.soy.exprtree.ExprNode;
import com.google.template.soy.exprtree.ExprRootNode;
import com.google.template.soy.exprtree.Operator;
import com.google.template.soy.pysrc.SoyPySrcOptions;
import com.google.template.soy.pysrc.SoyPySrcOptions.CodeStyle;
import com.google.template.soy.pysrc.internal.TranslateToPyExprVisitor.TranslateToPyExprVisitorFactory;
import com.google.template.soy.pysrc.internal.GenPyExprsVisitor.GenPyExprsVisitorFactory;
import com.google.template.soy.pysrc.restricted.PyCodeUtils;
import com.google.template.soy.pysrc.restricted.PyExpr;
import com.google.template.soy.pysrc.restricted.PyExprUtils;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.IsUsingIjData;
import com.google.template.soy.shared.restricted.ApiCallScopeBindingAnnotations.IsUsingDelPackages;
import com.google.template.soy.sharedpasses.ShouldEnsureDataIsDefinedVisitor;
import com.google.template.soy.soytree.AbstractSoyNodeVisitor;
import com.google.template.soy.soytree.CallNode;
import com.google.template.soy.soytree.CallParamContentNode;
import com.google.template.soy.soytree.CallParamNode;
import com.google.template.soy.soytree.DebuggerNode;
import com.google.template.soy.soytree.ForNode;
import com.google.template.soy.soytree.ForeachNode;
import com.google.template.soy.soytree.ForeachNonemptyNode;
import com.google.template.soy.soytree.IfCondNode;
import com.google.template.soy.soytree.IfElseNode;
import com.google.template.soy.soytree.IfNode;
import com.google.template.soy.soytree.LetContentNode;
import com.google.template.soy.soytree.LetValueNode;
import com.google.template.soy.soytree.LogNode;
import com.google.template.soy.soytree.MsgHtmlTagNode;
import com.google.template.soy.soytree.PrintNode;
import com.google.template.soy.soytree.SoyFileNode;
import com.google.template.soy.soytree.SoyFileSetNode;
import com.google.template.soy.soytree.SoyNode;
import com.google.template.soy.soytree.SoyNode.BlockNode;
import com.google.template.soy.soytree.SoyNode.ParentSoyNode;
import com.google.template.soy.soytree.SoySyntaxExceptionUtils;
import com.google.template.soy.soytree.SwitchCaseNode;
import com.google.template.soy.soytree.SwitchDefaultNode;
import com.google.template.soy.soytree.SwitchNode;
import com.google.template.soy.soytree.TemplateBasicNode;
import com.google.template.soy.soytree.TemplateDelegateNode;
import com.google.template.soy.soytree.TemplateNode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Visitor for generating full Python code (i.e. statements) for parse tree nodes.
 *
 * <p> Precondition: MsgNode should not exist in the tree.
 *
 * <p> {@link #exec} should be called on a full parse tree. Python source code will be generated for
 * all the Soy files. The return value is a list of strings, each string being the content of one
 * generated Python file (corresponding to one Soy file).
 *
 * @author Kai Huang
 */
class GenPyCodeVisitor extends AbstractSoyNodeVisitor<List<String>> {


  /** Regex pattern to look for dots in a template name. */
  private static final Pattern DOT = Pattern.compile("\\.");

  /** Regex pattern for an integer. */
  private static final Pattern INTEGER = Pattern.compile("-?\\d+");


  /** The options for generating Python source code. */
  private final SoyPySrcOptions pySrcOptions;

  /** Whether any of the Soy code uses injected data. */
  private final boolean isUsingIjData;

  /** Whether any of the Soy code uses delegate packages. */
  private final boolean isUsingDelPackages;

  /** Factory for creating an instance of TranslateToPyExprVisitor. */
  private final TranslateToPyExprVisitorFactory translateToPyExprVisitorFactory;

  /** Instance of GenCallCodeUtils to use. */
  private final GenCallCodeUtils genCallCodeUtils;

  /** The IsComputableAsPyExprsVisitor used by this instance. */
  private final IsComputableAsPyExprsVisitor isComputableAsPyExprsVisitor;

  /** The CanInitOutputVarVisitor used by this instance. */
  private final CanInitOutputVarVisitor canInitOutputVarVisitor;

  /** Factory for creating an instance of GenPyExprsVisitor. */
  private final GenPyExprsVisitorFactory genPyExprsVisitorFactory;

  /** The contents of the generated Python files. */
  private List<String> pyFilesContents;

  /** The PyCodeBuilder to build the current Python file being generated (during a run). */
  @VisibleForTesting protected PyCodeBuilder pyCodeBuilder;

  /** The current stack of replacement Python expressions for the local variables (and foreach-loop
   *  special functions) current in scope. */
  @VisibleForTesting protected Deque<Map<String, PyExpr>> localVarTranslations;

  /** The GenPyExprsVisitor used for the current template. */
  @VisibleForTesting protected GenPyExprsVisitor genPyExprsVisitor;


  /**
   * @param pySrcOptions The options for generating Python source code.
   * @param isUsingIjData Whether any of the Soy code uses injected data.
   * @param isUsingDelPackages Whether any of the Soy code uses delegate packages.
   * @param translateToPyExprVisitorFactory Instance of TranslateToPyExprVisitorFactory to use.
   * @param genCallCodeUtils Instance of GenCallCodeUtils to use.
   * @param isComputableAsPyExprsVisitor The IsComputableAsPyExprsVisitor to use.
   * @param canInitOutputVarVisitor The CanInitOutputVarVisitor to use.
   * @param genPyExprsVisitorFactory Factory for creating an instance of GenPyExprsVisitor.
   */
  @Inject
  GenPyCodeVisitor(
      SoyPySrcOptions pySrcOptions,
      @IsUsingIjData boolean isUsingIjData,
      @IsUsingDelPackages boolean isUsingDelPackages,
      TranslateToPyExprVisitorFactory translateToPyExprVisitorFactory,
      GenCallCodeUtils genCallCodeUtils,
      IsComputableAsPyExprsVisitor isComputableAsPyExprsVisitor,
      CanInitOutputVarVisitor canInitOutputVarVisitor,
      GenPyExprsVisitorFactory genPyExprsVisitorFactory) {
    this.pySrcOptions = pySrcOptions;
    this.isUsingIjData = isUsingIjData;
    this.isUsingDelPackages = isUsingDelPackages;
    this.translateToPyExprVisitorFactory = translateToPyExprVisitorFactory;
    this.genCallCodeUtils = genCallCodeUtils;
    this.isComputableAsPyExprsVisitor = isComputableAsPyExprsVisitor;
    this.canInitOutputVarVisitor = canInitOutputVarVisitor;
    this.genPyExprsVisitorFactory = genPyExprsVisitorFactory;
  }


  @Override public List<String> exec(SoyNode node) {
    pyFilesContents = Lists.newArrayList();
    pyCodeBuilder = null;
    localVarTranslations = null;
    genPyExprsVisitor = null;
    visit(node);
    return pyFilesContents;
  }


  /**
   * This method must only be called by assistant visitors.
   */
  void visitForUseByAssistants(SoyNode node) {
    visit(node);
  }


  @VisibleForTesting
  @Override protected void visit(SoyNode node) {
    super.visit(node);
  }


  @Override protected void visitChildren(ParentSoyNode<?> node) {

    // If the block is empty or if the first child cannot initilize the output var, we must
    // initialize the output var.
    if (node.numChildren() == 0 || !canInitOutputVarVisitor.exec(node.getChild(0))) {
      pyCodeBuilder.initOutputVarIfNecessary();
    }

    List<PyExpr> consecChildrenPyExprs = Lists.newArrayList();

    for (SoyNode child : node.getChildren()) {

      if (isComputableAsPyExprsVisitor.exec(child)) {
        consecChildrenPyExprs.addAll(genPyExprsVisitor.exec(child));

      } else {
        // We've reached a child that is not computable as Python expressions.

        // First add the PyExprs from preceding consecutive siblings that are computable as Python
        // expressions (if any).
        if (consecChildrenPyExprs.size() > 0) {
          pyCodeBuilder.addToOutputVar(consecChildrenPyExprs);
          consecChildrenPyExprs.clear();
        }

        // Now append the code for this child.
        visit(child);
      }
    }

    // Add the PyExprs from the last few children (if any).
    if (consecChildrenPyExprs.size() > 0) {
      pyCodeBuilder.addToOutputVar(consecChildrenPyExprs);
      consecChildrenPyExprs.clear();
    }
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for specific nodes.


  @Override protected void visitSoyFileSetNode(SoyFileSetNode node) {

    for (SoyFileNode soyFile : node.getChildren()) {
      try {
        visit(soyFile);
      } catch (SoySyntaxException sse) {
        throw sse.associateMetaInfo(null, soyFile.getFilePath(), null);
      }
    }
  }


  /**
   * Example:
   * <pre>
   * # This file was automatically generated from my-templates.soy.
   * # Please don't edit this file by hand.
   *
   * if (typeof boo == 'undefined') { var boo = {}; }
   * if (typeof boo.foo == 'undefined') { boo.foo = {}; }
   *
   * ...
   * </pre>
   */
  @Override protected void visitSoyFileNode(SoyFileNode node) {

    if (node.getSoyFileKind() == SoyFileKind.DEP) {
      return;  // don't generate code for deps
    }

    pyCodeBuilder = new PyCodeBuilder(pySrcOptions.getCodeStyle());

    pyCodeBuilder.appendLine("# This file was automatically generated from ",
                             node.getFileName(), ".");
    pyCodeBuilder.appendLine("# Please don't edit this file by hand.");

    // Add code to define Python namespaces or add provide/require calls for Closure Library.
    pyCodeBuilder.appendLine();
    addCodeToDefinePyNamespaces(node);

    // Add code for each template.
    for (TemplateNode template : node.getChildren()) {
      pyCodeBuilder.appendLine().appendLine();
      try {
        visit(template);
      } catch (SoySyntaxException sse) {
        throw sse.associateMetaInfo(null, null, template.getTemplateNameForUserMsgs());
      }
    }
    pyCodeBuilder.appendLine();

    pyFilesContents.add(pyCodeBuilder.getCode());
    pyCodeBuilder = null;
  }


  /**
   * Helper for visitSoyFileNode(SoyFileNode) to add code to define Python namespaces.
   * @param soyFile The node we're visiting.
   */
  private void addCodeToDefinePyNamespaces(SoyFileNode soyFile) {

    SortedSet<String> pyNamespaces = Sets.newTreeSet();
    for (TemplateNode template : soyFile.getChildren()) {
      String templateName = template.getTemplateName();
      Matcher dotMatcher = DOT.matcher(templateName);
      while (dotMatcher.find()) {
        pyNamespaces.add(templateName.substring(0, dotMatcher.start()));
      }
    }

    pyCodeBuilder.appendLine("import " + PyCodeUtils.UTILS_LIB);
    pyCodeBuilder.appendLine("import " + PyCodeUtils.DATA_LIB);
    pyCodeBuilder.appendLine("import soy.bidi");
    pyCodeBuilder.appendLine("import math");
    pyCodeBuilder.appendLine("import random");
    pyCodeBuilder.appendLine();

    pyCodeBuilder.appendLine("try:");
    pyCodeBuilder.appendLine("    registry");
    pyCodeBuilder.appendLine("except NameError:");
    pyCodeBuilder.appendLine("    raise ImportError('This file should be imported using soy.tofu.SoyTofu')");
    pyCodeBuilder.appendLine();
  }


  /**
   * Example:
   * <pre>
   * def my_func(data=None, sb=None):
   *     var output = '' if sb is None else []
   *     ...
   *     ...
   *     return '' if sb is not None else ''.join(output)
   * </pre>
   */
  @Override protected void visitTemplateNode(TemplateNode node) {

    boolean isCodeStyleStringbuilder = pySrcOptions.getCodeStyle() == CodeStyle.STRINGBUILDER;

    localVarTranslations = new ArrayDeque<Map<String, PyExpr>>();
    genPyExprsVisitor = genPyExprsVisitorFactory.create(localVarTranslations);

    // ------ Generate function definition up to opening brace. ------
    String pyTemplateName = node.getTemplateName().replace(".", "__");
    pyCodeBuilder.appendLine(
        "def ", pyTemplateName, "(data=None",
        (isCodeStyleStringbuilder ? ", sb=None" : ""),
        (isUsingIjData ? ", ijData=None" : ""),
        (isUsingDelPackages ? ", delPackages=None" : ""),
        "):");
    pyCodeBuilder.increaseIndent();

    // ------ Generate function body. ------
    generateFunctionBody(node);

    // ------ Generate function closing brace. ------
    pyCodeBuilder.decreaseIndent();

    // ------ Register the function. ------
    pyCodeBuilder.appendLine();
    pyCodeBuilder.appendLine(PyCodeUtils.genFunctionCall(
        "registry.register",
        PyCodeUtils.genAsciiStringLiteral(node.getTemplateName()),
        pyTemplateName));

    // ------ If delegate template, generate a statement to register it. ------
    if (node instanceof TemplateDelegateNode) {
      TemplateDelegateNode templateDelegateNode = (TemplateDelegateNode) node;
      pyCodeBuilder.appendLine(PyCodeUtils.genFunctionCall(
          "registry.registerDelegate",
          PyCodeUtils.genAsciiStringLiteral(templateDelegateNode.getParent().getDelPackageName()),
          PyCodeUtils.genAsciiStringLiteral(templateDelegateNode.getDelTemplateName()),
          PyCodeUtils.genAsciiStringLiteral(templateDelegateNode.getDelTemplateVariant()),
          PyCodeUtils.genIntLiteral(templateDelegateNode.getDelPriority()),
          pyTemplateName));
    }
  }


  /**
   * Generates the function body.
   */
  private void generateFunctionBody(TemplateNode node) {
    boolean isCodeStyleStringbuilder = pySrcOptions.getCodeStyle() == CodeStyle.STRINGBUILDER;

    localVarTranslations.push(Maps.<String, PyExpr>newHashMap());

    // Generate statement to ensure data is defined, if necessary.
    if ((new ShouldEnsureDataIsDefinedVisitor()).exec(node)) {
      pyCodeBuilder.appendLine("data = {} if data is None else data");
    }

    PyExpr resultPyExpr;
    if (!isCodeStyleStringbuilder && isComputableAsPyExprsVisitor.exec(node)) {
      // Case 1: The code style is 'concat' and the whole template body can be represented as Python
      // expressions. We specially handle this case because we don't want to generate the variable
      // 'output' at all. We simply concatenate the Python expressions and return the result.

      List<PyExpr> templateBodyPyExprs = genPyExprsVisitor.exec(node);
      resultPyExpr = PyExprUtils.concatPyExprs(templateBodyPyExprs);
    } else {
      // Case 2: Normal case.

      pyCodeBuilder.pushOutputVar("output");
      if (isCodeStyleStringbuilder) {
        pyCodeBuilder.appendLine("output = " + PyCodeUtils.genStringBuilder("") +" if sb is None else sb");
        pyCodeBuilder.setOutputVarInited();
      }

      visitChildren(node);

      if (isCodeStyleStringbuilder) {
        String resultText =
            PyCodeUtils.genStringLiteral("") +
            " if sb is not None else " +
            PyCodeUtils.genStringBuilderToString("output");
        resultPyExpr = new PyExpr(resultText, Integer.MAX_VALUE);
      } else {
        resultPyExpr = new PyExpr("output", Integer.MAX_VALUE);
      }
      pyCodeBuilder.popOutputVar();
    }

    if (node.getContentKind() != null) {
      if (isCodeStyleStringbuilder) {
        // TODO: In string builder mode, the only way to support strict is if callers know
        // whether their callees are strict and wrap at the call site. This is challenging
        // because most projects compile Javascript one file at a time. Since concat mode
        // is faster in most browsers nowadays, this may not be a priority.
        throw SoySyntaxExceptionUtils.createWithNode(
            "Soy's StringBuilder-based code generation mode does not currently support " +
            "autoescape=\"strict\".",
            node);
      }
      // Templates with autoescape="strict" return the SanitizedContent wrapper for its kind:
      // - Call sites are wrapped in an escaper. Returning SanitizedContent prevents re-escaping.
      // - The topmost call into Soy returns a SanitizedContent. This will make it easy to take
      // the result of one template and feed it to another, and also to confidently assign sanitized
      // HTML content to innerHTML.
      resultPyExpr = PyExprUtils.maybeWrapAsSanitizedContent(
          node.getContentKind(), resultPyExpr);
    }
    pyCodeBuilder.appendLine("return ", resultPyExpr.getText());

    localVarTranslations.pop();
  }

  @Override protected void visitMsgHtmlTagNode(MsgHtmlTagNode node) {
    throw new AssertionError();
  }


  @Override protected void visitPrintNode(PrintNode node) {
    pyCodeBuilder.addToOutputVar(genPyExprsVisitor.exec(node));
  }


  /**
   * Example:
   * <pre>
   *   {let $boo: $foo.goo[$moo] /}
   * </pre>
   * might generate
   * <pre>
   *   var boo35 = opt_data.foo.goo[opt_data.moo];
   * </pre>
   */
  @Override protected void visitLetValueNode(LetValueNode node) {

    String generatedVarName = node.getUniqueVarName();

    // Generate code to define the local var.
    PyExpr valuePyExpr =
        translateToPyExprVisitorFactory.create(localVarTranslations).exec(node.getValueExpr());
    pyCodeBuilder.appendLine(generatedVarName, " = ", valuePyExpr.getText());

    // Add a mapping for generating future references to this local var.
    localVarTranslations.peek().put(
        node.getVarName(), new PyExpr(generatedVarName, Integer.MAX_VALUE));
  }


  /**
   * Example:
   * <pre>
   *   {let $boo}
   *     Hello {$name}
   *   {/let}
   * </pre>
   * might generate
   * <pre>
   *   var boo35 = 'Hello ' + opt_data.name;
   * </pre>
   */
  @Override protected void visitLetContentNode(LetContentNode node) {

    String generatedVarName = node.getUniqueVarName();

    // Generate code to define the local var.
    localVarTranslations.push(Maps.<String, PyExpr>newHashMap());
    pyCodeBuilder.pushOutputVar(generatedVarName);

    visitChildren(node);

    pyCodeBuilder.popOutputVar();
    localVarTranslations.pop();

    if (pySrcOptions.getCodeStyle() == CodeStyle.STRINGBUILDER) {
      pyCodeBuilder.appendLine(generatedVarName, " = ", generatedVarName, ".toString()");
    }

    if (node.getContentKind() != null) {
      // If the let node had a content kind specified, it was autoescaped in the corresponding
      // context. Hence the result of evaluating the let block is wrapped in a SanitizedContent
      // instance of the appropriate kind.

      // The expression for the constructor of SanitizedContent of the appropriate kind (e.g.,
      // "soydata.VERY_UNSAFE.ordainSanitizedHtml"), or null if the node has no 'kind' attribute.
      final String sanitizedContentOrdainer =
          NodeContentKinds.toJsSanitizedContentOrdainer(node.getContentKind());

      pyCodeBuilder.appendLine(generatedVarName, " = ", sanitizedContentOrdainer, "(",
          generatedVarName, ")");
    }

    // Add a mapping for generating future references to this local var.
    localVarTranslations.peek().put(
        node.getVarName(), new PyExpr(generatedVarName, Integer.MAX_VALUE));
  }


  /**
   * Example:
   * <pre>
   *   {if $boo.foo &gt; 0}
   *     ...
   *   {/if}
   * </pre>
   * might generate
   * <pre>
   *   if (opt_data.boo.foo &gt; 0) {
   *     ...
   *   }
   * </pre>
   */
  @Override protected void visitIfNode(IfNode node) {

    if (isComputableAsPyExprsVisitor.exec(node)) {
      pyCodeBuilder.addToOutputVar(genPyExprsVisitor.exec(node));
      return;
    }

    // ------ Not computable as Python expressions, so generate full code. ------

    for (SoyNode child : node.getChildren()) {

      if (child instanceof IfCondNode) {
        IfCondNode icn = (IfCondNode) child;

        PyExpr condPyExpr =
            translateToPyExprVisitorFactory.create(localVarTranslations).exec(icn.getExprUnion().getExpr());
        if (icn.getCommandName().equals("if")) {
          pyCodeBuilder.appendLine("if ", condPyExpr.getText(), ":");
        } else {  // "elseif" block
          pyCodeBuilder.appendLine("elif ", condPyExpr.getText(), ":");
        }

        pyCodeBuilder.increaseIndent();
        visit(icn);
        pyCodeBuilder.decreaseIndent();

      } else if (child instanceof IfElseNode) {
        IfElseNode ien = (IfElseNode) child;

        pyCodeBuilder.appendLine("else:");

        pyCodeBuilder.increaseIndent();
        visit(ien);
        pyCodeBuilder.decreaseIndent();

      } else {
        throw new AssertionError();
      }
    }
  }


  /**
   * Example:
   * <pre>
   *   {switch $boo}
   *     {case 0}
   *       ...
   *     {case 1, 2}
   *       ...
   *     {default}
   *       ...
   *   {/switch}
   * </pre>
   * might generate
   * <pre>
   *   switch (opt_data.boo) {
   *     case 0:
   *       ...
   *       break;
   *     case 1:
   *     case 2:
   *       ...
   *       break;
   *     default:
   *       ...
   *   }
   * </pre>
   */
  @Override protected void visitSwitchNode(SwitchNode node) {
    String switchValueName = "switchValue" + node.getId();

    PyExpr switchValuePyExpr =
        translateToPyExprVisitorFactory.create(localVarTranslations).exec(node.getExpr());
    pyCodeBuilder.appendLine(switchValueName, " = ", switchValuePyExpr.getText());

    boolean isFirst = true;

    for (SoyNode child : node.getChildren()) {
      if (child instanceof SwitchCaseNode) {
        SwitchCaseNode scn = (SwitchCaseNode) child;

        for (ExprNode caseExpr : scn.getExprList()) {
          PyExpr casePyExpr =
              translateToPyExprVisitorFactory.create(localVarTranslations).exec(caseExpr);
          String ifOrElif = isFirst ? "if" : "elif";
          pyCodeBuilder.appendLine(ifOrElif, " ", switchValueName, " == ", casePyExpr.getText(), ":");

          pyCodeBuilder.increaseIndent();
          visit(scn);
          pyCodeBuilder.decreaseIndent();

          isFirst = false;
        }
      }
    }

    for (SoyNode child : node.getChildren()) {
      if (child instanceof SwitchCaseNode) {
        // nothing to do
      } else if (child instanceof SwitchDefaultNode) {
        SwitchDefaultNode sdn = (SwitchDefaultNode) child;

        String elseOrIsTrue = isFirst ? "if True" : "else";
        pyCodeBuilder.appendLine(elseOrIsTrue, ":");

        pyCodeBuilder.increaseIndent();
        visit(sdn);
        pyCodeBuilder.decreaseIndent();

      } else {
        throw new AssertionError();
      }
    }
  }


  /**
   * Example:
   * <pre>
   *   {foreach $foo in $boo.foos}
   *     ...
   *   {ifempty}
   *     ...
   *   {/foreach}
   * </pre>
   * might generate
   * <pre>
   *   fooList2 = data['boo']['foos']
   *   fooListLen2 = len(fooList2)
   *   if fooListLen2 > 0:
   *       ...
   *   else:
   *       ...
   * </pre>
   */
  @Override protected void visitForeachNode(ForeachNode node) {

    // Build some local variable names.
    String baseVarName = node.getVarName();
    String nodeId = Integer.toString(node.getId());
    String listVarName = baseVarName + "List" + nodeId;
    String listLenVarName = baseVarName + "ListLen" + nodeId;

    // Define list var and list-len var.
    PyExpr dataRefPyExpr =
        translateToPyExprVisitorFactory.create(localVarTranslations).exec(node.getExpr());
    pyCodeBuilder.appendLine(listVarName, " = ", dataRefPyExpr.getText());
    pyCodeBuilder.appendLine(listLenVarName, " = len(", listVarName, ")");

    // If has 'ifempty' node, add the wrapper 'if' statement.
    boolean hasIfemptyNode = node.numChildren() == 2;
    if (hasIfemptyNode) {
      pyCodeBuilder.appendLine("if ", listLenVarName, " > 0:");
      pyCodeBuilder.increaseIndent();
    }

    // Generate code for nonempty case.
    visit(node.getChild(0));

    // If has 'ifempty' node, add the 'else' block of the wrapper 'if' statement.
    if (hasIfemptyNode) {
      pyCodeBuilder.decreaseIndent();
      pyCodeBuilder.appendLine("else:");
      pyCodeBuilder.increaseIndent();

      // Generate code for empty case.
      visit(node.getChild(1));

      pyCodeBuilder.decreaseIndent();
    }
  }


  /**
   * Example:
   * <pre>
   *   {foreach $foo in $boo.foos}
   *     ...
   *   {/foreach}
   * </pre>
   * might generate
   * <pre>
   *   for fooIndex2, fooData2 in enumerate(fooList2):
   *       ...
   * </pre>
   */
  @Override protected void visitForeachNonemptyNode(ForeachNonemptyNode node) {

    // Build some local variable names.
    String baseVarName = node.getVarName();
    String foreachNodeId = Integer.toString(node.getForeachNodeId());
    String listVarName = baseVarName + "List" + foreachNodeId;
    String listLenVarName = baseVarName + "ListLen" + foreachNodeId;
    String indexVarName = baseVarName + "Index" + foreachNodeId;
    String dataVarName = baseVarName + "Data" + foreachNodeId;

    // The start of the Python 'for' loop.
    pyCodeBuilder.appendLine("for ", indexVarName, ", ", dataVarName, " in enumerate(", listVarName, "):");
    pyCodeBuilder.increaseIndent();

    // Add a new localVarTranslations frame and populate it with the translations from this node.
    Map<String, PyExpr> newLocalVarTranslationsFrame = Maps.newHashMap();
    newLocalVarTranslationsFrame.put(
        baseVarName, new PyExpr(dataVarName, Integer.MAX_VALUE));
    newLocalVarTranslationsFrame.put(
        baseVarName + "__isFirst",
        new PyExpr(indexVarName + " == 0", Operator.EQUAL.getPrecedence()));
    newLocalVarTranslationsFrame.put(
        baseVarName + "__isLast",
        new PyExpr(indexVarName + " == " + listLenVarName + " - 1",
            Operator.EQUAL.getPrecedence()));
    newLocalVarTranslationsFrame.put(
        baseVarName + "__index", new PyExpr(indexVarName, Integer.MAX_VALUE));
    localVarTranslations.push(newLocalVarTranslationsFrame);

    // Generate the code for the loop body.
    visitChildren(node);

    // Remove the localVarTranslations frame that we added above.
    localVarTranslations.pop();

    // The end of the Python 'for' loop.
    pyCodeBuilder.decreaseIndent();
  }


  /**
   * Example:
   * <pre>
   *   {for $i in range(1, $boo)}
   *     ...
   *   {/for}
   * </pre>
   * might generate
   * <pre>
   *   iLimit4 = data['boo'];
   *   for i4 in xrange(1, iLimit4):
   *       ...
   * </pre>
   */
  @Override protected void visitForNode(ForNode node) {

    String varName = node.getVarName();
    String nodeId = Integer.toString(node.getId());

    // Get the Python expression text for the init/limit/increment values.
    List<ExprRootNode<?>> rangeArgs = Lists.newArrayList(node.getRangeArgs());
    String incrementPyExprText =
        (rangeArgs.size() == 3) ?
        translateToPyExprVisitorFactory.create(localVarTranslations).exec(rangeArgs.remove(2))
            .getText() :
        "1" /* default */;
    String initPyExprText =
        (rangeArgs.size() == 2) ?
        translateToPyExprVisitorFactory.create(localVarTranslations).exec(rangeArgs.remove(0))
            .getText() :
        "0" /* default */;
    String limitPyExprText =
        translateToPyExprVisitorFactory.create(localVarTranslations).exec(rangeArgs.get(0))
            .getText();

    // The start of the Python 'for' loop.
    pyCodeBuilder.appendLine(
        "for ", varName, nodeId, " in xrange(", initPyExprText, ", ", limitPyExprText, ", ", incrementPyExprText, "):");
    pyCodeBuilder.increaseIndent();

    // Add a new localVarTranslations frame and populate it with the translations from this node.
    Map<String, PyExpr> newLocalVarTranslationsFrame = Maps.newHashMap();
    newLocalVarTranslationsFrame.put(varName, new PyExpr(varName + nodeId, Integer.MAX_VALUE));
    localVarTranslations.push(newLocalVarTranslationsFrame);

    // Generate the code for the loop body.
    visitChildren(node);

    // Remove the localVarTranslations frame that we added above.
    localVarTranslations.pop();

    // The end of the Python 'for' loop.
    pyCodeBuilder.decreaseIndent();
  }


  /**
   * Example:
   * <pre>
   *   {call some.func data="all" /}
   *   {call some.func data="$boo.foo" /}
   *   {call some.func}
   *     {param goo: 88 /}
   *   {/call}
   *   {call some.func data="$boo"}
   *     {param goo}
   *       Hello {$name}
   *     {/param}
   *   {/call}
   * </pre>
   * might generate
   * <pre>
   *   output += some.func(opt_data);
   *   output += some.func(opt_data.boo.foo);
   *   output += some.func({goo: 88});
   *   output += some.func(soy.$$augmentMap(opt_data.boo, {goo: 'Hello ' + opt_data.name});
   * </pre>
   */
  @Override protected void visitCallNode(CallNode node) {

    // If this node has any CallParamContentNode children those contents are not computable as Python
    // expressions, visit them to generate code to define their respective 'param<n>' variables.
    for (CallParamNode child : node.getChildren()) {
      if (child instanceof CallParamContentNode && !isComputableAsPyExprsVisitor.exec(child)) {
        visit(child);
      }
    }

    if (pySrcOptions.getCodeStyle() == CodeStyle.STRINGBUILDER) {
      // For 'stringbuilder' code style, pass the current output var to collect the call's output.
      genCallCodeUtils.genAndAppendCallStmt(pyCodeBuilder, node, localVarTranslations);

    } else {
      // For 'concat' code style, we simply add the call's result to the current output var.
      PyExpr callExpr = genCallCodeUtils.genCallExpr(node, localVarTranslations);
      pyCodeBuilder.addToOutputVar(ImmutableList.of(callExpr));
    }
  }


  @Override protected void visitCallParamContentNode(CallParamContentNode node) {

    // This node should only be visited when it's not computable as Python expressions, because this
    // method just generates the code to define the temporary 'param<n>' variable.
    if (isComputableAsPyExprsVisitor.exec(node)) {
      throw new AssertionError(
          "Should only define 'param<n>' when not computable as Python expressions.");
    }

    localVarTranslations.push(Maps.<String, PyExpr>newHashMap());
    pyCodeBuilder.pushOutputVar("param" + node.getId());

    visitChildren(node);

    pyCodeBuilder.popOutputVar();
    localVarTranslations.pop();
  }


  /**
   * Example:
   * <pre>
   *   {log}Blah {$boo}.{/log}
   * </pre>
   * might generate
   * <pre>
   *   print u'Blah ' + data['boo'] + '.'
   * </pre>
   *
   * <p> If the log msg is not computable as Python exprs, then it will be built in a local var
   * logMsg_s##, e.g.
   * <pre>
   *   logMsg_s14 = ...
   *   print logMsg_s14
   * </pre>
   */
  @Override protected void visitLogNode(LogNode node) {

    if (isComputableAsPyExprsVisitor.execOnChildren(node)) {
      List<PyExpr> logMsgPyExprs = genPyExprsVisitor.execOnChildren(node);
      PyExpr logMsgPyExpr = PyExprUtils.concatPyExprs(logMsgPyExprs);
      pyCodeBuilder.appendLine("print ", logMsgPyExpr.getText());

    } else {
      // Must build log msg in a local var logMsg_s##.
      localVarTranslations.push(Maps.<String, PyExpr>newHashMap());
      pyCodeBuilder.pushOutputVar("logMsg_s" + node.getId());

      visitChildren(node);

      pyCodeBuilder.popOutputVar();
      localVarTranslations.pop();

      pyCodeBuilder.appendLine("print logMsg_s", Integer.toString(node.getId()));
    }
  }


  /**
   * Example:
   * <pre>
   *   {debugger}
   * </pre>
   * generates
   * <pre>
   * </pre>
   */
  @Override protected void visitDebuggerNode(DebuggerNode node) {
  }


  // -----------------------------------------------------------------------------------------------
  // Fallback implementation.


  @Override protected void visitSoyNode(SoyNode node) {

    if (node instanceof ParentSoyNode<?>) {

      if (node instanceof BlockNode) {
        localVarTranslations.push(Maps.<String, PyExpr>newHashMap());
        visitChildren((BlockNode) node);
        localVarTranslations.pop();

      } else {
        visitChildren((ParentSoyNode<?>) node);
      }

      return;
    }

    if (isComputableAsPyExprsVisitor.exec(node)) {
      // Simply generate Python expressions for this node and add them to the current output var.
      pyCodeBuilder.addToOutputVar(genPyExprsVisitor.exec(node));

    } else {
      // Need to implement visit*Node() for the specific case.
      throw new UnsupportedOperationException();
    }
  }

}
