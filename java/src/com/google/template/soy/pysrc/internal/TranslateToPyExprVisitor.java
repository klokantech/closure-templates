/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.template.soy.pysrc.internal;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.google.template.soy.base.BaseUtils;
import com.google.template.soy.base.SoySyntaxException;
import com.google.template.soy.exprtree.AbstractReturningExprNodeVisitor;
import com.google.template.soy.exprtree.DataRefAccessIndexNode;
import com.google.template.soy.exprtree.DataRefAccessKeyNode;
import com.google.template.soy.exprtree.DataRefAccessNode;
import com.google.template.soy.exprtree.DataRefNode;
import com.google.template.soy.exprtree.ExprNode;
import com.google.template.soy.exprtree.ExprNode.ConstantNode;
import com.google.template.soy.exprtree.ExprNode.OperatorNode;
import com.google.template.soy.exprtree.ExprNode.PrimitiveNode;
import com.google.template.soy.exprtree.ExprRootNode;
import com.google.template.soy.exprtree.FunctionNode;
import com.google.template.soy.exprtree.GlobalNode;
import com.google.template.soy.exprtree.ListLiteralNode;
import com.google.template.soy.exprtree.MapLiteralNode;
import com.google.template.soy.exprtree.Operator;
import com.google.template.soy.exprtree.OperatorNodes;
import com.google.template.soy.exprtree.OperatorNodes.AndOpNode;
import com.google.template.soy.exprtree.OperatorNodes.NotOpNode;
import com.google.template.soy.exprtree.OperatorNodes.OrOpNode;
import com.google.template.soy.exprtree.StringNode;
import com.google.template.soy.pysrc.restricted.PyCodeUtils;
import com.google.template.soy.pysrc.restricted.PyExpr;
import com.google.template.soy.pysrc.restricted.SoyPyCodeUtils;
import com.google.template.soy.pysrc.restricted.SoyPySrcFunction;
import com.google.template.soy.shared.internal.NonpluginFunction;

import java.util.Deque;
import java.util.List;
import java.util.Map;


/**
 * Visitor for translating a Soy expression (in the form of an {@code ExprNode}) into an
 * equivalent Python expression.
 *
 * <p> Important: Do not use outside of Soy code (treat as superpackage-private).
 *
 * @author Kai Huang
 */
public class TranslateToPyExprVisitor extends AbstractReturningExprNodeVisitor<PyExpr> {


  /**
   * Injectable factory for creating an instance of this class.
   */
  public static interface TranslateToPyExprVisitorFactory {

    /**
     * @param localVarTranslations The current stack of replacement Python expressions for the local
     *     variables (and foreach-loop special functions) current in scope.
     */
    public TranslateToPyExprVisitor create(Deque<Map<String, PyExpr>> localVarTranslations);
  }


  /** Map of all SoyPySrcFunctions (name to function). */
  private final Map<String, SoyPySrcFunction> soyPySrcFunctionsMap;

  /** The current stack of replacement Python expressions for the local variables (and foreach-loop
   *  special functions) current in scope. */
  private final Deque<Map<String, PyExpr>> localVarTranslations;


  /**
   * @param soyPySrcFunctionsMap Map of all SoyPySrcFunctions (name to function).
   * @param localVarTranslations The current stack of replacement Python expressions for the local
   *     variables (and foreach-loop special functions) current in scope.
   */
  @AssistedInject
  TranslateToPyExprVisitor(
      Map<String, SoyPySrcFunction> soyPySrcFunctionsMap,
      @Assisted Deque<Map<String, PyExpr>> localVarTranslations) {
    this.soyPySrcFunctionsMap = soyPySrcFunctionsMap;
    this.localVarTranslations = localVarTranslations;
  }


  // -----------------------------------------------------------------------------------------------
  // Implementation for a dummy root node.


  @Override protected PyExpr visitExprRootNode(ExprRootNode<?> node) {
    return visit(node.getChild(0));
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for primitives.


  @Override protected PyExpr visitStringNode(StringNode node) {
    // Note: StringNode.toSourceString() produces a Soy string, which is usually a valid Python string.
    // The rare exception is a string containing a Unicode Format character (Unicode category "Cf")
    // because of the Python language quirk that requires all category "Cf" characters to be
    // escaped in Python strings. Therefore, we must call PySrcUtils.escapeUnicodeFormatChars() on the
    // result.
    return new PyExpr(
        PySrcUtils.escapeUnicodeFormatChars(node.toSourceString()),
        Integer.MAX_VALUE);
  }


  @Override protected PyExpr visitPrimitiveNode(PrimitiveNode node) {
    // Note: ExprNode.toSourceString() technically returns a Soy expression. In the case of
    // primitives, the result is usually also the correct Python expression.
    // Note: The rare exception to the above note is a StringNode containing a Unicode Format
    // character (Unicode category "Cf") because of the Python language quirk that requires all
    // category "Cf" characters to be escaped in Python strings. Therefore, we have a separate
    // implementation above for visitStringNode(StringNode).
    return new PyExpr(node.toSourceString(), Integer.MAX_VALUE);
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for collections.


  @Override protected PyExpr visitListLiteralNode(ListLiteralNode node) {

    StringBuilder exprTextSb = new StringBuilder();
    exprTextSb.append('[');

    boolean isFirst = true;
    for (ExprNode child : node.getChildren()) {
      if (isFirst) {
        isFirst = false;
      } else {
        exprTextSb.append(", ");
      }
      exprTextSb.append(visit(child).getText());
    }

    exprTextSb.append(']');

    return new PyExpr(exprTextSb.toString(), Integer.MAX_VALUE);
  }


  @Override protected PyExpr visitMapLiteralNode(MapLiteralNode node) {
    return visitMapLiteralNodeHelper(node, false);
  }


  /**
   * Helper to visit a MapLiteralNode, with the extra option of whether to quote keys.
   */
  private PyExpr visitMapLiteralNodeHelper(MapLiteralNode node, boolean doQuoteKeys) {

    // If there are only string keys, then the expression will be
    //     {aa: 11, bb: 22}    or    {'aa': 11, 'bb': 22}
    // where the former is with unquoted keys and the latter with quoted keys.
    // If there are both string and nonstring keys, then the expression will be
    //     (function() { var map_s = {'aa': 11}; map_s[opt_data.bb] = 22; return map_s; })()

    StringBuilder strKeysEntriesSnippet = new StringBuilder();
    StringBuilder nonstrKeysEntriesSnippet = new StringBuilder();

    for (int i = 0, n = node.numChildren(); i < n; i += 2) {
      ExprNode keyNode = node.getChild(i);
      ExprNode valueNode = node.getChild(i + 1);

      if (keyNode instanceof StringNode) {
        if (strKeysEntriesSnippet.length() > 0) {
          strKeysEntriesSnippet.append(", ");
        }
        if (doQuoteKeys) {
          strKeysEntriesSnippet.append(visit(keyNode).getText());
        } else {
          String key = ((StringNode) keyNode).getValue();
          if (BaseUtils.isIdentifier(key)) {
            strKeysEntriesSnippet.append(key);
          } else {
            strKeysEntriesSnippet.append(visit(keyNode).getText());
          }
        }
        strKeysEntriesSnippet.append(": ").append(visit(valueNode).getText());

      } else if (keyNode instanceof ConstantNode) {
        throw SoySyntaxException.createWithoutMetaInfo(
            "Map literal must have keys that are strings or expressions that will evaluate to" +
                " strings at render time (found non-string key \"" + keyNode.toSourceString() +
                "\" in map literal \"" + node.toSourceString() + "\").");

      } else {
        nonstrKeysEntriesSnippet
            .append(" map_s[soy.$$checkMapKey(").append(visit(keyNode).getText()).append(")] = ")
            .append(visit(valueNode).getText()).append(';');
      }
    }

    String fullExprText;
    if (nonstrKeysEntriesSnippet.length() == 0) {
      fullExprText = "{" + strKeysEntriesSnippet.toString() + "}";
    } else {
      fullExprText = "(function() { var map_s = {" + strKeysEntriesSnippet.toString() + "};" +
          nonstrKeysEntriesSnippet.toString() + " return map_s; })()";
    }

    return new PyExpr(fullExprText, Integer.MAX_VALUE);
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for data references.


  @Override protected PyExpr visitDataRefNode(DataRefNode node) {

    // Note: Using String instead of StringBuilder for readability. No performance concern here.
    String refText;

    // ------ Translate first key, which may reference a variable, data, or injected data. ------
    String firstKey = node.getFirstKey();
    if (node.isIjDataRef()) {
      // Case 1: Injected data reference.
      refText = genGetDataSingleByKey("ijData", firstKey);
    } else {
      PyExpr translation = getLocalVarTranslation(firstKey);
      if (translation != null) {
        // Case 2: In-scope local var.
        refText = translation.getText();
      } else {
        // Case 3: Data reference.
        refText = genGetDataSingleByKey("data", firstKey);
      }
    }

    // ------ Translate the rest of the keys, if any. ------
    for (ExprNode child : node.getChildren()) {
      DataRefAccessNode accessNode = (DataRefAccessNode) child;

      switch (accessNode.getKind()) {
        case DATA_REF_ACCESS_KEY_NODE:
          refText = genGetDataSingleByKey(refText, ((DataRefAccessKeyNode) accessNode).getKey());
          break;
        case DATA_REF_ACCESS_INDEX_NODE:
          refText = genGetDataSingleByIndex(refText, ((DataRefAccessIndexNode) accessNode).getIndex());
          break;
        case DATA_REF_ACCESS_EXPR_NODE:
          refText = genGetDataSingleByExpr(refText, visit(accessNode.getChild(0)));
          break;
        default:
          throw new AssertionError();
      }
    }

    return new PyExpr(refText, Integer.MAX_VALUE);
  }


  @Override protected PyExpr visitGlobalNode(GlobalNode node) {
    return new PyExpr(node.toSourceString(), Integer.MAX_VALUE);
  }


  // -----------------------------------------------------------------------------------------------
  // Implementations for operators.


  @Override protected PyExpr visitNotOpNode(NotOpNode node) {
    return genPyExprUsingSoySyntaxWithNewToken(node, "not");
  }


  @Override protected PyExpr visitAndOpNode(AndOpNode node) {
    return genPyExprUsingSoySyntaxWithNewToken(node, "and");
  }


  @Override protected PyExpr visitOrOpNode(OrOpNode node) {
    return genPyExprUsingSoySyntaxWithNewToken(node, "or");
  }


  @Override protected PyExpr visitOperatorNode(OperatorNode node) {
    return genPyExprUsingSoySyntax(node);
  }

  @Override protected PyExpr visitConditionalOpNode(OperatorNodes.ConditionalOpNode node) {
    PyExpr operand0 = visit(node.getChild(0));
    PyExpr operand1 = visit(node.getChild(1));
    PyExpr operand2 = visit(node.getChild(2));

    String condText = operand1.getText() + " if " + operand0.getText() + " else " + operand2.getText();
    return new PyExpr(condText, Operator.CONDITIONAL.getPrecedence());
  }




  // -----------------------------------------------------------------------------------------------
  // Implementations for functions.


  @Override protected PyExpr visitFunctionNode(FunctionNode node) {

    String fnName = node.getFunctionName();
    int numArgs = node.numChildren();

    // Handle nonplugin functions.
    NonpluginFunction nonpluginFn = NonpluginFunction.forFunctionName(fnName);
    if (nonpluginFn != null) {
      if (numArgs != nonpluginFn.getNumArgs()) {
        throw SoySyntaxException.createWithoutMetaInfo(
            "Function '" + fnName + "' called with the wrong number of arguments" +
                " (function call \"" + node.toSourceString() + "\").");
      }
      switch (nonpluginFn) {
        case IS_FIRST:
          return visitIsFirstFunction(node);
        case IS_LAST:
          return visitIsLastFunction(node);
        case INDEX:
          return visitIndexFunction(node);
        case QUOTE_KEYS_IF_JS:
          return visitMapLiteralNodeHelper((MapLiteralNode) node.getChild(0), true);
        default:
          throw new AssertionError();
      }
    }

    // Handle plugin functions.
    SoyPySrcFunction fn = soyPySrcFunctionsMap.get(fnName);
    if (fn != null) {
      if (! fn.getValidArgsSizes().contains(numArgs)) {
        throw SoySyntaxException.createWithoutMetaInfo(
            "Function '" + fnName + "' called with the wrong number of arguments" +
                " (function call \"" + node.toSourceString() + "\").");
      }
      List<PyExpr> args = visitChildren(node);
      try {
        return fn.computeForPySrc(args);
      } catch (Exception e) {
        throw SoySyntaxException.createCausedWithoutMetaInfo(
            "Error in function call \"" + node.toSourceString() + "\": " + e.getMessage(), e);
      }
    }

    // Function not found.
    throw SoySyntaxException.createWithoutMetaInfo(
        "Failed to find SoyPySrcFunction with name '" + fnName + "'" +
            " (function call \"" + node.toSourceString() + "\").");
  }


  private PyExpr visitIsFirstFunction(FunctionNode node) {
    String varName = ((DataRefNode) node.getChild(0)).getFirstKey();
    return getLocalVarTranslation(varName + "__isFirst");
  }


  private PyExpr visitIsLastFunction(FunctionNode node) {
    String varName = ((DataRefNode) node.getChild(0)).getFirstKey();
    return getLocalVarTranslation(varName + "__isLast");
  }


  private PyExpr visitIndexFunction(FunctionNode node) {
    String varName = ((DataRefNode) node.getChild(0)).getFirstKey();
    return getLocalVarTranslation(varName + "__index");
  }


  // -----------------------------------------------------------------------------------------------
  // Private helpers.

  private String genGetDataSingleByKey(String dataExprText, String key) {
    return PyCodeUtils.genFunctionCall(
        PyCodeUtils.UTILS_LIB + ".getDataSingleAttr",
        dataExprText, PyCodeUtils.genAsciiStringLiteral(key));
  }

  private String genGetDataSingleByIndex(String dataExprText, int index) {
    return PyCodeUtils.genFunctionCall(
        PyCodeUtils.UTILS_LIB + ".getDataSingleItem",
        dataExprText, PyCodeUtils.genIntLiteral(index));
  }

  private String genGetDataSingleByExpr(String dataExprText, PyExpr expr) {
    return PyCodeUtils.genFunctionCall(
        PyCodeUtils.UTILS_LIB + ".getDataSingleItem",
        dataExprText, expr.getText());
  }

  /**
   * Gets the translated expression for an in-scope local variable (or special "variable" derived
   * from a foreach-loop var), or null if not found.
   * @param ident The Soy local variable to translate.
   * @return The translated expression for the given variable, or null if not found.
   */
  private PyExpr getLocalVarTranslation(String ident) {

    for (Map<String, PyExpr> localVarTranslationsFrame : localVarTranslations) {
      PyExpr translation = localVarTranslationsFrame.get(ident);
      if (translation != null) {
        return translation;
      }
    }

    return null;
  }


  /**
   * Generates a Python expression for the given OperatorNode's subtree assuming that the Python expression
   * for the operator uses the same syntax format as the Soy operator.
   * @param opNode The OperatorNode whose subtree to generate a Python expression for.
   * @return The generated Python expression.
   */
  private PyExpr genPyExprUsingSoySyntax(OperatorNode opNode) {
    return genPyExprUsingSoySyntaxWithNewToken(opNode, null);
  }


  /**
   * Generates a Python expression for the given OperatorNode's subtree assuming that the Python expression
   * for the operator uses the same syntax format as the Soy operator, with the exception that the
   * Python operator uses a different token (e.g. "!" instead of "not").
   * @param opNode The OperatorNode whose subtree to generate a Python expression for.
   * @param newToken The equivalent Python operator's token.
   * @return The generated Python expression.
   */
  private PyExpr genPyExprUsingSoySyntaxWithNewToken(OperatorNode opNode, String newToken) {

    List<PyExpr> operandPyExprs = visitChildren(opNode);

    return SoyPyCodeUtils.genPyExprUsingSoySyntaxWithNewToken(
        opNode.getOperator(), operandPyExprs, newToken);
  }

}
